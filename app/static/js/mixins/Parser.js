export default {
    methods: {
        parseExcel: function(file, success) {
            var reader = new FileReader()

            reader.onload = function(e) {
                var data = e.target.result
                var workbook = XLSX.read(data, {
                    type: 'binary',
                })

                var sheets = []

                workbook.SheetNames.forEach(function(sheetName) {
                    // Here is your object
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName], { raw: true })

                    sheets.push({
                        name: sheetName,
                        data: XL_row_object,
                    })
                })

                success(sheets)
            }

            reader.onerror = function(ex) {
                console.log(ex)
            }

            reader.readAsBinaryString(file)
        },
    },
}
