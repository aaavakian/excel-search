import Main from './components/Main.js'

// Parser mixin
import Parser from './mixins/Parser.js'

Vue.mixin(Parser)

new Vue({
    el: '#app',
    render: h => h(Main),
})
