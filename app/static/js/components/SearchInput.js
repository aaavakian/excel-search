export default {
    name: 'SearchInput',
    props: {
        searchText: {
            type: String,
            default: '',
        },
    },
    methods: {
        search: function() {
            this.$emit('searchRequested', this.searchText)
        },
        clear: function() {
            this.searchText = ''
            this.search()
        },
    },
    template: `
        <div class="row mt-4">
            <div class="col-lg-6 offset-lg-3">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Введите запрос..." v-model="searchText" @keyup.enter="search"/>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" @click="clear">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <div class="input-group-append">
                        <button class="btn btn-primary" @click="search">Поиск</button>
                    </div>
                </div>
            </div>
        </div>
    `,
}
