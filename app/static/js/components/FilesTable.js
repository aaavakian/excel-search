export default {
    name: 'FilesTable',
    props: {
        files: {
            type: Array,
            required: true,
        },
    },
    template: `
        <div class="row mt-4">
            <div class="col">
                <table class="table table-bordered table-striped m-0" id="filesTable">
                    <thead>
                        <tr>
                            <th>Имя файла</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template v-if="files.length">
                            <tr v-for="(file, i) in files" :key="i">
                                <td>{{ file.name }}</td>
                            </tr>
                        </template>
                        <tr v-else>
                            <td class="text-center">Файлы не выбраны</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `,
}