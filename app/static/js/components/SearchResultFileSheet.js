export default {
    name: 'SearchResultFileSheet',
    props: {
        sheet: {
            type: Object,
            required: true,
        },
        searchText: {
            type: String,
            default: '',
        },
    },
    data: function() {
        return {
            sortKey: null,
        }
    },
    computed: {
        searchParts: function() {
            return this.searchText.toLowerCase().split(/\s/g)
        },
        foundItems: function() {
            if (this.sortKey) {
                this.sheet.data.sort((a, b) => {
                    return a[this.sortKey] < b[this.sortKey] ? -1 : 1
                })
            }

            return this.sheet.data.filter(data => {
                // Remove all whites spaces in cells and join columns
                var solidCells = Object.values(data).map(cell => cell.toString().replace(/\s/g, '')),
                    rowText = solidCells.join(' ').toLowerCase()
                return this.searchParts.every(searchText => rowText.includes(searchText))
            })
        },
    },
    methods: {
        sortByKey: function(column) {
            this.sortKey = column
        },
    },
    template: `
        <div class="mt-4">
            <p class="font-weight-bold">{{ sheet.name }}</p>
            <table class="table table-sm m-0" v-if="foundItems.length"> <!-- table-responsive -->
                <thead>
                    <tr>
                        <th style="cursor: pointer;" v-for="(column, i) in Object.keys(foundItems[0])" :key="i" @click="sortByKey(column)">
                            <span :class="{ 'text-success': sortKey == column }">{{ column }}</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(row, i) in foundItems" :key="i">
                        <td v-for="(value, key) in row" :key="key">{{ value }}</td>
                    </tr>
                </tbody>
            </table>
            <p class="text-danger" v-else>Нет результатов</p>
        </div>
    `,
}
