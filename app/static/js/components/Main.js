// Components
import FileInput from './FileInput.js'
import FilesTable from './FilesTable.js'
import SearchInput from './SearchInput.js'
import SearchResult from './SearchResult.js'

export default {
    data: function() {
        return {
            files: [],
            searchText: '',
        }
    },
    computed: {
        filesNumber: function() {
            return this.files.length
        },
    },
    components: {
        FileInput,
        FilesTable,
        SearchInput,
        SearchResult,
    },
    template: `
        <main class="py-4">
            <div class="container">
                <h2>Поиск по Excel файлам</h2>
                <file-input :filesNumber="filesNumber" @filesSelected="loadFiles"></file-input>
                <files-table :files="files"></files-table>
                <search-input :searchText="searchText" @searchRequested="searchRows"></search-input>
                <search-result :files="files" :searchText="searchText"></search-result>
            </div>
        </main>
    `,
    methods: {
        loadFiles: function(files) {
            // Clear files
            this.files = []
            // Clear search
            this.searchText = ''
            
            // Parse and define files
            for (let index = 0; index < files.length; index++) {
                const file = files[index]

                this.parseExcel(file, sheets => {
                    console.log('Success for ' + file.name)

                    // Add to files
                    this.files.push({
                        name: file.name,
                        sheets: sheets,
                    })
                })
            }
        },
        searchRows: function(searchText) {
            this.searchText = searchText
        },
    }
}
