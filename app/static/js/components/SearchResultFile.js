import SearchResultFileSheet from './SearchResultFileSheet.js'

export default {
    name: 'SearchResultFile',
    props: {
        file: {
            type: Object,
            required: true,
        },
        searchText: {
            type: String,
            default: '',
        },
    },
    computed: {
        sheets: function() {
            return this.file.sheets.filter(sheet => sheet.data.length > 0)
        },
    },
    components: {
        SearchResultFileSheet,
    },
    template: `
        <div class="row mt-4">
            <div class="col">
                <h5 class="text-primary">{{ file.name }}</h5>
                <template v-if="sheets.length">
                    <search-result-file-sheet
                        v-for="(sheet, i) in sheets"
                        :key="i"
                        :sheet="sheet"
                        :searchText="searchText">
                    </search-result-file-sheet>
                </template>
                <p class="text-danger" v-else>В файле нет данных</p>
            </div>
        </div>
    `,
}
