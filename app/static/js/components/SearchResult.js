import SearchResultFile from './SearchResultFile.js'

export default {
    name: 'SearchResult',
    props: {
        files: {
            type: Array,
            required: true,
        },
        searchText: {
            type: String,
            required: true,
        },
    },
    components: {
        SearchResultFile,
    },
    template: `
        <section v-if="files && searchText">
            <search-result-file v-for="file in files" :file="file" :searchText="searchText"/>
        </section>
    `,
}
