export default {
    name: 'FileInput',
    props: {
        filesNumber: {
            type: Number,
            default: 0,
        },
    },
    computed: {
        placeholder: function() {
            return this.filesNumber ? ('Выбрано файлов: ' + this.filesNumber) : 'Выберите Excel файлы...'
        },
    },
    template: `
        <div class="row mt-4">
            <div class="col-lg-6 offset-lg-3">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="excelFile" accept=".csv, .xls, .xlsx" multiple @change="filesSelected"/>
                    <label class="custom-file-label" for="excelFile">{{ placeholder }}</label>
                </div>
            </div>
        </div>
    `,
    methods: {
        filesSelected: function(e) {
            this.$emit('filesSelected', e.target.files)
        },
    },
}
